# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  arr.max-arr.min
  # your code goes here
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  # your code goes here
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  # your code goes here
  str.downcase!
  vowels = 'aeiou'.split('')
  count = 0
  str.split('').each { | char | count += 1 if vowels.include?(char) }
  count
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  # your code goes here
  str.downcase!
  str.delete('aeiou')
end



# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  digits = int.to_s.split('')
  digits.sort.reverse
  # your code goes here
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  # your code goes here
  str.downcase!
  return false if str.split('').length == str.split('').uniq.length
  true
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  # your code goes here
  arr.map!{|num| num.to_s}

  first = arr[0..2].join
  middle = arr[3..5].join
  last = arr[6..-1].join

  "(#{first}) #{middle}-#{last}"

end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  # your code goes here
  nums = str.split(',').map {|str| str.to_i}
  return 0 if nums.max == nums.min
  (nums.min..nums.max).to_a.length-1

end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  # your code goes here
  return negative_rotate(arr, offset) if offset < 0
  offset.times do
    first = arr.shift
    arr << first
  end

  arr

end

def negative_rotate(arr, offset)
  offset = offset.abs
  offset.times do
    last = arr.pop
    arr.unshift(last)
  end

  arr
end
